package com.itau.unibanco.acesso.repository;

import com.itau.unibanco.acesso.models.Acesso;
import org.springframework.data.repository.CrudRepository;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {
}

