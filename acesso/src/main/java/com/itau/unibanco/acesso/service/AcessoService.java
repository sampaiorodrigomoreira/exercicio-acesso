package com.itau.unibanco.acesso.service;

import com.itau.unibanco.acesso.DTOs.AcessoDTO;
import com.itau.unibanco.acesso.clients.ClienteClient;
import com.itau.unibanco.acesso.clients.PortaClient;
import com.itau.unibanco.acesso.clients.exceptions.ClienteNotFoundException;
import com.itau.unibanco.acesso.clients.exceptions.PortaNotFoundException;
import com.itau.unibanco.acesso.models.Acesso;
import com.itau.unibanco.acesso.models.Cliente;
import com.itau.unibanco.acesso.models.Porta;
import com.itau.unibanco.acesso.repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    ClienteClient clienteClient;

    @Autowired
    PortaClient portaClient;

    @Autowired
    AcessoRepository acessoRepository;

    public AcessoDTO cadastrarAcesso(AcessoDTO acessoDTO) {

        Optional<Porta> portaOptional = portaClient.getPorta(acessoDTO.getPortaId());

        if(portaOptional.isPresent()){

            Optional<Cliente> clienteOptional = clienteClient.getCliente(acessoDTO.getClienteId());

            if(clienteOptional.isPresent()){
                Acesso novoAcesso = new Acesso(
                        portaOptional.get().getId(),
                        clienteOptional.get().getId()

                );
                acessoRepository.save(novoAcesso);
                return acessoDTO;

            } else {
                throw new ClienteNotFoundException();
            }
        } else {
            throw new PortaNotFoundException();
        }
    }



}
