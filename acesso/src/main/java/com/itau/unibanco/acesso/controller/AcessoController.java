package com.itau.unibanco.acesso.controller;

import com.itau.unibanco.acesso.DTOs.AcessoDTO;
import com.itau.unibanco.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoDTO cadastraAcesso(@RequestBody AcessoDTO acessoDTO) {
        return acessoService.cadastrarAcesso(acessoDTO);
    }

}

