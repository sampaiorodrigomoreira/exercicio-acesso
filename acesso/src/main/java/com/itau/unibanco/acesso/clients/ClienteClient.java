package com.itau.unibanco.acesso.clients;

import com.itau.unibanco.acesso.clients.decoder.cliente.ClienteClientConfiguration;
import com.itau.unibanco.acesso.models.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {
    @GetMapping("/cliente/{id}")
    Optional<Cliente> getCliente(@PathVariable int id);
}
