package com.itau.unibanco.acesso.clients;

import com.itau.unibanco.acesso.models.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "porta", configuration = PortaClient.class)
public interface PortaClient {
    @GetMapping("/porta/{id}")
    Optional<Porta> getPorta(@PathVariable int id);
}
