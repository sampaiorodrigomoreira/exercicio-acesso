package com.itau.unibanco.cliente.repository;

import com.itau.unibanco.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
