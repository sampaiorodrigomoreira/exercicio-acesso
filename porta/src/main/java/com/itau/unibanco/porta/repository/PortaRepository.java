package com.itau.unibanco.porta.repository;

import com.itau.unibanco.porta.models.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Integer> {
}
