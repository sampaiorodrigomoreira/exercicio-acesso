package com.itau.unibanco.porta.controllers;

import com.itau.unibanco.porta.models.Porta;
import com.itau.unibanco.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta criarPorta(@RequestBody @Valid Porta porta){
        Porta objPorta = portaService.salvarPorta(porta);
        return objPorta;
    }

    @GetMapping("/{id}")
    public Porta pesquisarPorId(@PathVariable(name = "id") int id){
        try{
            Porta porta = portaService.buscarPortaPeloId(id);
            return porta;
        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
