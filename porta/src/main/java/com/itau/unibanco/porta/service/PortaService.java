package com.itau.unibanco.porta.service;

import com.itau.unibanco.porta.models.Porta;
import com.itau.unibanco.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta salvarPorta(Porta porta){
        Porta objPorta = portaRepository.save(porta);
        return objPorta;
    }

    public Porta buscarPortaPeloId(int id){
        Optional<Porta> portaOptional = portaRepository.findById(id);
        if(portaOptional.isPresent()){
            Porta porta = portaOptional.get();
            return porta;
        }else{
            throw new RuntimeException("Porta não encontrada");
        }
    }

}
